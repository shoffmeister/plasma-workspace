# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
#
# Andrej Mernik <andrejm@ubuntu.si>, 2014, 2015, 2016, 2017, 2018.
# Matjaž Jeran <matjaz.jeran@amis.net>, 2020, 2021, 2022, 2023.
# Martin Srebotnjak <miles@filmsi.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-08 01:36+0000\n"
"PO-Revision-Date: 2023-11-22 07:13+0100\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Andrej Mernik <andrejm@ubuntu.si>\n"
"X-Generator: Poedit 3.4.1\n"

#: ../sddm-theme/KeyboardButton.qml:19
#, kde-format
msgid "Keyboard Layout: %1"
msgstr "Razporeditev tipk: %1"

#: ../sddm-theme/Login.qml:85
#, kde-format
msgid "Username"
msgstr "Uporabniško ime"

#: ../sddm-theme/Login.qml:102 contents/lockscreen/MainBlock.qml:64
#, kde-format
msgid "Password"
msgstr "Geslo"

#: ../sddm-theme/Login.qml:144 ../sddm-theme/Login.qml:150
#, kde-format
msgid "Log In"
msgstr "Prijavi se"

#: ../sddm-theme/Main.qml:201 contents/lockscreen/LockScreenUi.qml:277
#, kde-format
msgid "Caps Lock is on"
msgstr "Caps Lock je vključen"

#: ../sddm-theme/Main.qml:213 ../sddm-theme/Main.qml:357
#: contents/logout/Logout.qml:167
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "V pripravljenost"

#: ../sddm-theme/Main.qml:220 ../sddm-theme/Main.qml:364
#: contents/logout/Logout.qml:188 contents/logout/Logout.qml:199
#, kde-format
msgid "Restart"
msgstr "Znova zaženi"

#: ../sddm-theme/Main.qml:227 ../sddm-theme/Main.qml:371
#: contents/logout/Logout.qml:212
#, kde-format
msgid "Shut Down"
msgstr "Izklopi"

#: ../sddm-theme/Main.qml:234
#, kde-format
msgctxt "For switching to a username and password prompt"
msgid "Other…"
msgstr "Ostali…"

#: ../sddm-theme/Main.qml:343
#, kde-format
msgid "Type in Username and Password"
msgstr "Vnesite uporabniško ime in geslo"

#: ../sddm-theme/Main.qml:378
#, kde-format
msgid "List Users"
msgstr "Seznam uporabnikov"

#: ../sddm-theme/Main.qml:453 contents/lockscreen/LockScreenUi.qml:364
#, kde-format
msgctxt "Button to show/hide virtual keyboard"
msgid "Virtual Keyboard"
msgstr "Navidezna tipkovnica"

#: ../sddm-theme/Main.qml:527
#, kde-format
msgid "Login Failed"
msgstr "Neuspešna prijava"

#: ../sddm-theme/SessionButton.qml:18
#, kde-format
msgid "Desktop Session: %1"
msgstr "Seja namizja: %1"

#: contents/lockscreen/config.qml:19
#, kde-format
msgctxt "@title: group"
msgid "Clock:"
msgstr "Ura:"

#: contents/lockscreen/config.qml:22
#, kde-format
msgctxt "@option:check"
msgid "Keep visible when unlocking prompt disappears"
msgstr "Ohrani vidno, ko poziv za odklepanje izgine"

#: contents/lockscreen/config.qml:33
#, kde-format
msgctxt "@title: group"
msgid "Media controls:"
msgstr "Upravljanje medijev:"

#: contents/lockscreen/config.qml:36
#, kde-format
msgctxt "@option:check"
msgid "Show under unlocking prompt"
msgstr "Kaži pod pozivom za odklepanje"

#: contents/lockscreen/LockScreenUi.qml:50
#, kde-format
msgid "Unlocking failed"
msgstr "Odklepanje ni uspelo"

#: contents/lockscreen/LockScreenUi.qml:291
#, kde-format
msgid "Sleep"
msgstr "Spanje"

#: contents/lockscreen/LockScreenUi.qml:297 contents/logout/Logout.qml:177
#, kde-format
msgid "Hibernate"
msgstr "V hibernacijo"

#: contents/lockscreen/LockScreenUi.qml:303
#, kde-format
msgid "Switch User"
msgstr "Preklopi uporabnika"

#: contents/lockscreen/LockScreenUi.qml:388
#, kde-format
msgctxt "Button to change keyboard layout"
msgid "Switch layout"
msgstr "Preklopi razporeditev"

#: contents/lockscreen/MainBlock.qml:107
#: contents/lockscreen/NoPasswordUnlock.qml:17
#, kde-format
msgid "Unlock"
msgstr "Odkleni"

#: contents/lockscreen/MainBlock.qml:155
#, kde-format
msgid "(or scan your fingerprint on the reader)"
msgstr "(ali skenirajte svoj prstni odtis na bralniku)"

#: contents/lockscreen/MainBlock.qml:159
#, kde-format
msgid "(or scan your smartcard)"
msgstr "(ali skenirajte svojo pametno kartico)"

#: contents/lockscreen/MediaControls.qml:59
#, kde-format
msgid "No title"
msgstr "Brez naslova"

#: contents/lockscreen/MediaControls.qml:60
#, kde-format
msgid "No media playing"
msgstr "Nič se ne predvaja"

#: contents/lockscreen/MediaControls.qml:88
#, kde-format
msgid "Previous track"
msgstr "Predhodna sled"

#: contents/lockscreen/MediaControls.qml:100
#, kde-format
msgid "Play or Pause media"
msgstr "Predvajaj ali prekini predvajanje medija"

#: contents/lockscreen/MediaControls.qml:110
#, kde-format
msgid "Next track"
msgstr "Naslednja sled"

#: contents/logout/Logout.qml:142
#, kde-format
msgid ""
"One other user is currently logged in. If the computer is shut down or "
"restarted, that user may lose work."
msgid_plural ""
"%1 other users are currently logged in. If the computer is shut down or "
"restarted, those users may lose work."
msgstr[0] ""
"Še %1 uporabnik je trenutno prijavljen. Če bi bil računalnik ustavili ali "
"ponovno zagnali, bi lahko ta uporabnik izgubil opravljeno delo na "
"računalniku."
msgstr[1] ""
"Še %1 uporabnika sta trenutno prijavljena. Če bi bil računalnik ustavili ali "
"ponovno zagnali, bi lahko ta uporabnika izgubila opravljeno delo na "
"računalniku."
msgstr[2] ""
"Še %1 uporabniki so trenutno prijavljeni. Če bi bil računalnik ustavili ali "
"ponovno zagnali, bi lahko ti uporabniki izgubili opravljeno delo na "
"računalniku."
msgstr[3] ""
"Še %1 uporabnikov je trenutno prijavljeno. Če bi bil računalnik ustavili ali "
"ponovno zagnali, bi lahko ti uporabniki izgubili opravljeno delo na "
"računalniku."

#: contents/logout/Logout.qml:156
#, kde-format
msgid "When restarted, the computer will enter the firmware setup screen."
msgstr ""
"Po ponovnem zagonu računalnik vstopi v zaslon za nastavitev strojne "
"programske opreme."

#: contents/logout/Logout.qml:187
#, kde-format
msgctxt "@action:button Keep short"
msgid "Install Updates & Restart"
msgstr "Namesti posodobitve in ponovno zaženi"

#: contents/logout/Logout.qml:223
#, kde-format
msgid "Log Out"
msgstr "Odjavi se"

#: contents/logout/Logout.qml:247
#, kde-format
msgid "Installing software updates and restarting in 1 second"
msgid_plural "Installing software updates and restarting in %1 seconds"
msgstr[0] "Nameščanje posodobitev programja in ponovni zagon v %1 sekundi"
msgstr[1] "Nameščanje posodobitev programja in ponovni zagon v %1 sekundah"
msgstr[2] "Nameščanje posodobitev programja in ponovni zagon v %1 sekundah"
msgstr[3] "Nameščanje posodobitev programja in ponovni zagon v %1 sekundah"

#: contents/logout/Logout.qml:248
#, kde-format
msgid "Restarting in 1 second"
msgid_plural "Restarting in %1 seconds"
msgstr[0] "Ponovni zagon čez %1 sekundo"
msgstr[1] "Ponovni zagon čez %1 sekundi"
msgstr[2] "Ponovni zagon čez %1 sekunde"
msgstr[3] "Ponovni zagon čez %1 sekund"

#: contents/logout/Logout.qml:250
#, kde-format
msgid "Shutting down in 1 second"
msgid_plural "Shutting down in %1 seconds"
msgstr[0] "Izklapljanje čez %1 sekundo"
msgstr[1] "Izklapljanje čez %1 sekundi"
msgstr[2] "Izklapljanje čez %1 sekunde"
msgstr[3] "Izklapljanje čez %1 sekund"

#: contents/logout/Logout.qml:252
#, kde-format
msgid "Logging out in 1 second"
msgid_plural "Logging out in %1 seconds"
msgstr[0] "Odjavljanje čez %1 sekundo"
msgstr[1] "Odjavljanje čez %1 sekundi"
msgstr[2] "Odjavljanje čez %1 sekunde"
msgstr[3] "Odjavljanje čez %1 sekund"

#: contents/logout/Logout.qml:263
#, kde-format
msgid "OK"
msgstr "OK"

#: contents/logout/Logout.qml:275
#, kde-format
msgid "Cancel"
msgstr "Prekliči"

#: contents/osd/OsdItem.qml:32
#, kde-format
msgctxt "Percentage value"
msgid "%1%"
msgstr "%1%"

#: contents/splash/Splash.qml:79
#, kde-format
msgctxt ""
"This is the first text the user sees while starting in the splash screen, "
"should be translated as something short, is a form that can be seen on a "
"product. Plasma is the project name so shouldn't be translated."
msgid "Plasma made by KDE"
msgstr "Plasma izdelana v skupnosti KDE"

#~ msgid "Switch to This Session"
#~ msgstr "Preklopi na to sejo"

#~ msgid "Start New Session"
#~ msgstr "Začni novo sejo"

#~ msgid "Back"
#~ msgstr "Nazaj"

#~ msgid "%1%"
#~ msgstr "%1 %"

#~ msgid "Battery at %1%"
#~ msgstr "Baterija na %1 %"

#~ msgctxt "Nobody logged in on that session"
#~ msgid "Unused"
#~ msgstr "Neuporabljeno"

#~ msgctxt "User logged in on console number"
#~ msgid "TTY %1"
#~ msgstr "TTY %1"

#~ msgctxt "User logged in on console (X display number)"
#~ msgid "on TTY %1 (Display %2)"
#~ msgstr "na TTY %1 (%2. zaslon)"

#~ msgctxt "Username (location)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#~ msgid "100%"
#~ msgstr "100%"
