# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# SPDX-FileCopyrightText: 2023 Vit Pelcak <vit@pelcak.org>
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-19 01:58+0000\n"
"PO-Revision-Date: 2023-11-06 09:59+0100\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.08.2\n"

#: devicenotifications.cpp:275
#, kde-format
msgid "%1 has been plugged in."
msgstr "%1 bylo připojeno."

#: devicenotifications.cpp:275
#, kde-format
msgid "A USB device has been plugged in."
msgstr "Zařízení USB bylo připojeno."

#: devicenotifications.cpp:278
#, kde-format
msgctxt "@title:notifications"
msgid "USB Device Detected"
msgstr "Bylo nalezeno zařízení USB"

#: devicenotifications.cpp:295
#, kde-format
msgid "%1 has been unplugged."
msgstr "%1 bylo odpojeno."

#: devicenotifications.cpp:295
#, kde-format
msgid "A USB device has been unplugged."
msgstr "Zařízení USB bylo odpojeno."

#: devicenotifications.cpp:298
#, kde-format
msgctxt "@title:notifications"
msgid "USB Device Removed"
msgstr "Zařízení USB bylo odebráno"
