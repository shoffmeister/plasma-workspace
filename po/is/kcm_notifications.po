# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Sveinn í Felli <sv1@fellsnet.is>, 2022.
# Guðmundur Erlingsson <gudmundure@gmail.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-16 01:38+0000\n"
"PO-Revision-Date: 2023-09-09 09:55+0000\n"
"Last-Translator: Guðmundur Erlingsson <gudmundure@gmail.com>\n"
"Language-Team: Icelandic <kde-i18n-doc@kde.org>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"

#: kcm.cpp:73
#, kde-format
msgid "Toggle do not disturb"
msgstr "Kveikja/slökkva á 'Ekki ónáða'"

#: sourcesmodel.cpp:393
#, kde-format
msgid "Other Applications"
msgstr "Önnur forrit"

#: ui/ApplicationConfiguration.qml:91
#, kde-format
msgid "Show popups"
msgstr "Birta sprettiglugga"

#: ui/ApplicationConfiguration.qml:105
#, kde-format
msgid "Show in do not disturb mode"
msgstr "Birta þegar stillt á 'Ekki ónáða'"

#: ui/ApplicationConfiguration.qml:118 ui/main.qml:147
#, kde-format
msgid "Show in history"
msgstr "Birta í ferilskráningu"

#: ui/ApplicationConfiguration.qml:129
#, kde-format
msgid "Show notification badges"
msgstr "Sýna tilkynningamerki"

#: ui/ApplicationConfiguration.qml:164
#, kde-format
msgctxt "@title:table Configure individual notification events in an app"
msgid "Configure Events"
msgstr "Grunnstilla viðburði"

#: ui/ApplicationConfiguration.qml:172
#, kde-format
msgid ""
"This application does not support configuring notifications on a per-event "
"basis"
msgstr ""
"Þetta forrit styður ekki grunnstillingu tilkynninga fyrir hvern viðburð "
"fyrir sig"

#: ui/ApplicationConfiguration.qml:261
#, kde-format
msgid "Show a message in a pop-up"
msgstr "Birta skilaboð í sprettiglugga"

#: ui/ApplicationConfiguration.qml:270
#, kde-format
msgid "Play a sound"
msgstr "Spila hljóð"

#: ui/main.qml:41
#, kde-format
msgid ""
"Could not find a 'Notifications' widget, which is required for displaying "
"notifications. Make sure that it is enabled either in your System Tray or as "
"a standalone widget."
msgstr ""
"Ekki tókst að finna græjuna 'Tilkynningar' ('Notifications') sem er krafist "
"til að hægt sé að birta tilkynningar. Gakktu úr skugga um að hún sé virkjuð "
"í kerfisbakkanum eða sem sjálfstæð græja."

#: ui/main.qml:52
#, kde-format
msgctxt "Vendor and product name"
msgid "Notifications are currently provided by '%1 %2' instead of Plasma."
msgstr "'%1 %2' útvegar tilkynningar sem stendur í stað Plasma."

#: ui/main.qml:56
#, kde-format
msgid "Notifications are currently not provided by Plasma."
msgstr "Plasma útvegar ekki tilkynningar sem stendur."

#: ui/main.qml:63
#, kde-format
msgctxt "@title:group"
msgid "Do Not Disturb mode"
msgstr "Stillingin 'Ekki ónáða':"

#: ui/main.qml:68
#, kde-format
msgctxt "Enable Do Not Disturb mode when screens are mirrored"
msgid "Enable:"
msgstr "Virkja:"

#: ui/main.qml:69
#, kde-format
msgctxt "Enable Do Not Disturb mode when screens are mirrored"
msgid "When screens are mirrored"
msgstr "Þegar skjáir eru speglaðir"

#: ui/main.qml:81
#, kde-format
msgctxt "Enable Do Not Disturb mode during screen sharing"
msgid "During screen sharing"
msgstr "Þegar skjár er samnýttur"

#: ui/main.qml:96
#, kde-format
msgctxt "Keyboard shortcut to turn Do Not Disturb mode on and off"
msgid "Keyboard shortcut:"
msgstr "Flýtilykill:"

#: ui/main.qml:103
#, kde-format
msgctxt "@title:group"
msgid "Visibility conditions"
msgstr "Skilyrði birtingar"

#: ui/main.qml:108
#, kde-format
msgid "Critical notifications:"
msgstr "Áríðandi tilkynningar:"

#: ui/main.qml:109
#, kde-format
msgid "Show in Do Not Disturb mode"
msgstr "Birta þegar stillt á 'Ekki ónáða'"

#: ui/main.qml:121
#, kde-format
msgid "Normal notifications:"
msgstr "Venjulegar tilkynningar:"

#: ui/main.qml:122
#, kde-format
msgid "Show over full screen windows"
msgstr "Birta yfir gluggum sem fylla skjáinn"

#: ui/main.qml:134
#, kde-format
msgid "Low priority notifications:"
msgstr "Tilkynningar án forgangs:"

#: ui/main.qml:135
#, kde-format
msgid "Show popup"
msgstr "Birta sprettiglugga"

#: ui/main.qml:164
#, kde-format
msgctxt "@title:group As in: 'notification popups'"
msgid "Popups"
msgstr "Sprettigluggar"

#: ui/main.qml:170
#, kde-format
msgctxt "@label"
msgid "Location:"
msgstr "Staðsetning:"

#: ui/main.qml:171
#, kde-format
msgctxt "Popup position near notification plasmoid"
msgid "Near notification icon"
msgstr "Nálægt tilkynningatákni"

#: ui/main.qml:208
#, kde-format
msgid "Choose Custom Position…"
msgstr "Velja sérsniðna staðsetningu..."

#: ui/main.qml:217 ui/main.qml:233
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 sekúnda"
msgstr[1] "%1 sekúndur"

#: ui/main.qml:222
#, kde-format
msgctxt "Part of a sentence like, 'Hide popup after n seconds'"
msgid "Hide after:"
msgstr "Fela eftir:"

#: ui/main.qml:245
#, kde-format
msgctxt "@title:group"
msgid "Additional feedback"
msgstr "Viðbótarendurgjöf"

#: ui/main.qml:251
#, kde-format
msgctxt "Show application jobs in notification widget"
msgid "Show in notifications"
msgstr "Birta í tilkynningum"

#: ui/main.qml:252
#, kde-format
msgid "Application progress:"
msgstr "Framvinda forrits:"

#: ui/main.qml:266
#, kde-format
msgctxt "Keep application job popup open for entire duration of job"
msgid "Keep popup open during progress"
msgstr "Halda sprettigluggum opnum í framvindu"

#: ui/main.qml:279
#, kde-format
msgid "Notification badges:"
msgstr "Tilkynningamerki:"

#: ui/main.qml:280
#, kde-format
msgid "Show in task manager"
msgstr "Birta í verkefnastjóra"

#: ui/main.qml:291
#, kde-format
msgctxt "@title:group"
msgid "Application-specific settings"
msgstr "Stillingar fyrir tiltekin forrit"

#: ui/main.qml:296
#, kde-format
msgid "Configure…"
msgstr "Grunnstilla…"

#: ui/PopupPositionPage.qml:14
#, kde-format
msgid "Popup Position"
msgstr "Staðsetning sprettiglugga"

#: ui/SourcesPage.qml:19
#, kde-format
msgid "Application Settings"
msgstr "Stillingar forrits"

#: ui/SourcesPage.qml:99
#, kde-format
msgid "Applications"
msgstr "Forrit"

#: ui/SourcesPage.qml:100
#, kde-format
msgid "System Services"
msgstr "Kerfisþjónustur"

#: ui/SourcesPage.qml:148
#, kde-format
msgid "No application or event matches your search term"
msgstr "Ekkert forrit eða viðburður passar við leitarorðið þitt."

#: ui/SourcesPage.qml:171
#, kde-format
msgid ""
"Select an application from the list to configure its notification settings "
"and behavior"
msgstr "Veldu forrit af listanum til að grunnstilla tilkynningar og hegðun."

#~ msgid "Configure Notifications"
#~ msgstr "Grunnstilla tilkynningar"

#~ msgid "This module lets you manage application and system notifications."
#~ msgstr ""
#~ "Með þessari einingu geturðu stjórnað tilkynningum forrita og kerfis."

#~ msgctxt "Turn do not disturb mode on/off with keyboard shortcut"
#~ msgid "Toggle with:"
#~ msgstr "Víxla með:"
