# translation of kcm_autostart.po to galician
# Copyright (C) 2008 This_file_is_part_of_KDE
# This file is distributed under the same license as the kcm_autostart package.
# mvillarino <mvillarino@users.sourceforge.net>, 2008, 2009.
# Miguel Branco <mgl.branco@gmail.com>, 2008.
# marce villarino <mvillarino@users.sourceforge.net>, 2009.
# Marce Villarino <mvillarino@kde-espana.es>, 2011, 2012.
# Adrián Chaves Fernández (Gallaecio) <adriyetichaves@gmail.com>, 2015, 2017.
# SPDX-FileCopyrightText: 2023 Adrián Chaves (Gallaecio)
#
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-18 01:59+0000\n"
"PO-Revision-Date: 2023-10-28 10:39+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.2\n"

#: autostartmodel.cpp:376
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr "«%1» non é un URL absoluto."

#: autostartmodel.cpp:379
#, kde-format
msgid "\"%1\" does not exist."
msgstr "«%1» non existe."

#: autostartmodel.cpp:382
#, kde-format
msgid "\"%1\" is not a file."
msgstr "«%1» non é un ficheiro."

#: autostartmodel.cpp:385
#, kde-format
msgid "\"%1\" is not readable."
msgstr "«%1» non é lexíbel."

#: ui/entry.qml:52
#, kde-format
msgctxt ""
"@label The name of a Systemd unit for an app or script that will autostart"
msgid "Name:"
msgstr "Nome:"

#: ui/entry.qml:57
#, kde-format
msgctxt ""
"@label The current status (e.g. active or inactive) of a Systemd unit for an "
"app or script that will autostart"
msgid "Status:"
msgstr "Estado:"

#: ui/entry.qml:62
#, kde-format
msgctxt ""
"@label A date and time follows this text, making a sentence like 'Last "
"activated on: August 7th 11 PM 2023'"
msgid "Last activated on:"
msgstr "Última activación:"

#: ui/entry.qml:71
#, kde-format
msgctxt "@label Stop the Systemd unit for a running process"
msgid "Stop"
msgstr "Deter"

#: ui/entry.qml:71
#, kde-format
msgctxt "@label Start the Systemd unit for a currently inactive process"
msgid "Start"
msgstr "Iniciar"

#: ui/entry.qml:105
#, kde-format
msgid "Unable to load logs. Try refreshing."
msgstr "Non é posíbel cargar os rexistros. Probe a actualizar."

#: ui/entry.qml:109
#, kde-format
msgctxt "@action:button Refresh entry logs when it failed to load"
msgid "Refresh"
msgstr "Actualizar"

#: ui/main.qml:33
#, kde-format
msgid "Make Executable"
msgstr "Facer executábel"

#: ui/main.qml:53
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr "O ficheiro «%1» debe ser executábel para executarse ao saír."

#: ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr "O ficheiro «%1» debe ser executábel para executarse ao acceder."

#: ui/main.qml:66
#, kde-format
msgctxt "@action:button"
msgid "Add…"
msgstr "Engadir…"

#: ui/main.qml:69
#, kde-format
msgctxt "@action:button"
msgid "Add Application…"
msgstr "Engadir unha aplicación…"

#: ui/main.qml:74
#, kde-format
msgctxt "@action:button"
msgid "Add Login Script…"
msgstr "Engadir un guión de acceso…"

#: ui/main.qml:79
#, kde-format
msgctxt "@action:button"
msgid "Add Logout Script…"
msgstr "Engadir un guión de saída…"

#: ui/main.qml:114
#, kde-format
msgid ""
"%1 has not been autostarted yet. Details will be available after the system "
"is restarted."
msgstr ""
"%1 aínda non se iniciou automaticamente. Haberá máis información despois de "
"reiniciar o sistema."

#: ui/main.qml:137
#, kde-format
msgctxt ""
"@label Entry hasn't been autostarted because system hasn't been restarted"
msgid "Not autostarted yet"
msgstr "Aínda non se iniciou automaticamente"

#: ui/main.qml:146
#, kde-format
msgctxt "@action:button"
msgid "See properties"
msgstr "Ver as propiedades"

#: ui/main.qml:154
#, kde-format
msgctxt "@action:button"
msgid "Remove entry"
msgstr "Retirar a entrada"

#: ui/main.qml:167
#, kde-format
msgid "Applications"
msgstr "Aplicacións"

#: ui/main.qml:170
#, kde-format
msgid "Login Scripts"
msgstr "Guións de inicio"

#: ui/main.qml:173
#, kde-format
msgid "Pre-startup Scripts"
msgstr "Guións de pre-arranque"

#: ui/main.qml:176
#, kde-format
msgid "Logout Scripts"
msgstr "Guións de saída"

#: ui/main.qml:184
#, kde-format
msgid "No user-specified autostart items"
msgstr "Non hai elementos de inicio automático indicados pola persoa usuaria."

#: ui/main.qml:185
#, kde-kuit-format
msgctxt "@info 'some' refers to autostart items"
msgid "Click the <interface>Add…</interface> button to add some"
msgstr "Prema o botón <interface>Engadir…</interface> para engadilos."

#: ui/main.qml:200
#, kde-format
msgid "Choose Login Script"
msgstr "Escolla un guión de inicio"

#: ui/main.qml:220
#, kde-format
msgid "Choose Logout Script"
msgstr "Escoller un guión de saída"

#: unit.cpp:26
#, kde-format
msgctxt "@label Entry is running right now"
msgid "Running"
msgstr "En execución"

#: unit.cpp:27
#, kde-format
msgctxt "@label Entry is not running right now (exited without error)"
msgid "Not running"
msgstr "Non en execución"

#: unit.cpp:28
#, kde-format
msgctxt "@label Entry is being started"
msgid "Starting"
msgstr "Iniciándose"

#: unit.cpp:29
#, kde-format
msgctxt "@label Entry is being stopped"
msgid "Stopping"
msgstr "Deténdose"

#: unit.cpp:30
#, kde-format
msgctxt "@label Entry has failed (exited with an error)"
msgid "Failed"
msgstr "Fallou"

#: unit.cpp:83
#, kde-format
msgid "Error occurred when receiving reply of GetAll call %1"
msgstr "Produciuse un erro ao recibir a resposta da chamada a GetAll %1."

#: unit.cpp:155
#, kde-format
msgid "Failed to open journal"
msgstr "Non foi posíbel abrir o diario."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Marce Villarino"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "mvillarino@users.sourceforge.net"

#~ msgid "Session Autostart Manager Control Panel Module"
#~ msgstr "Módulo de control dos inicios automáticos de sesión"

#, fuzzy
#~| msgid "Copyright © 2006–2010 Autostart Manager team"
#~ msgid "Copyright © 2006–2020 Autostart Manager team"
#~ msgstr "© 2006–2010 O equipo do Xestor de inicios automáticos"

#~ msgid "Stephen Leaf"
#~ msgstr "Stephen Leaf"

#~ msgid "Montel Laurent"
#~ msgstr "Montel Laurent"

#~ msgid "Maintainer"
#~ msgstr "Mantenedor"

#, fuzzy
#~| msgid "Advanced..."
#~ msgid "Add..."
#~ msgstr "Avanzado…"

#~ msgid "Shell script path:"
#~ msgstr "Ruta ao script da shell:"

#~ msgid "Create as symlink"
#~ msgstr "Crear como ligazón simbólica"

#~ msgid "Autostart only in Plasma"
#~ msgstr "Iniciar automaticamente só en Plasma"

#~ msgid "Command"
#~ msgstr "Orde"

#~ msgctxt ""
#~ "@title:column The name of the column that decides if the program is run "
#~ "on session startup, on session shutdown, etc"
#~ msgid "Run On"
#~ msgstr "Executar ao"

#~ msgid "Session Autostart Manager"
#~ msgstr "Xestor de inicios automáticos de sesión"

#~ msgctxt "The program will be run"
#~ msgid "Enabled"
#~ msgstr "Activado"

#~ msgctxt "The program won't be run"
#~ msgid "Disabled"
#~ msgstr "Desactivado"

#~ msgid "Desktop File"
#~ msgstr "Ficheiro de escritorio"

#~ msgid "Script File"
#~ msgstr "Ficheiro de script"

#~ msgid "Add Program..."
#~ msgstr "Engadir un programa…"

#~ msgid "Before session startup"
#~ msgstr "Antes do inicio da sesión"

#~ msgid ""
#~ "Only files with “.sh” extensions are allowed for setting up the "
#~ "environment."
#~ msgstr ""
#~ "Só se permite configurar o ambiente con ficheiros con extensión «.sh»."

#~ msgid "Shutdown"
#~ msgstr "Apagar"
