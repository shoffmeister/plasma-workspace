# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Yasen Pramatarov <yasen@lindeas.com>, 2009, 2010, 2011.
# SPDX-FileCopyrightText: 2022, 2023 Mincho Kondarev <mkondarev@yahoo.de>
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_devicenotifier\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-02 01:37+0000\n"
"PO-Revision-Date: 2023-12-11 11:39+0100\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#: package/contents/ui/DeviceItem.qml:189
#, kde-format
msgctxt "@info:status Free disk space"
msgid "%1 free of %2"
msgstr "%1 свободни от %2"

#: package/contents/ui/DeviceItem.qml:193
#, kde-format
msgctxt ""
"Accessing is a less technical word for Mounting; translation should be short "
"and mean 'Currently mounting this device'"
msgid "Accessing…"
msgstr "Осъществяване на достъп…"

#: package/contents/ui/DeviceItem.qml:196
#, kde-format
msgctxt ""
"Removing is a less technical word for Unmounting; translation should be "
"short and mean 'Currently unmounting this device'"
msgid "Removing…"
msgstr "Премахване…"

#: package/contents/ui/DeviceItem.qml:199
#, kde-format
msgid "Don't unplug yet! Files are still being transferred…"
msgstr "Не отделяйте устройството! Все още се прехвърлят файлове…"

#: package/contents/ui/DeviceItem.qml:230
#, kde-format
msgid "Open in File Manager"
msgstr "Отваряне във Файлов мениджър"

#: package/contents/ui/DeviceItem.qml:233
#, kde-format
msgid "Mount and Open"
msgstr "Монтиране и отваряне"

#: package/contents/ui/DeviceItem.qml:235
#, kde-format
msgid "Eject"
msgstr "Изваждане"

#: package/contents/ui/DeviceItem.qml:237
#, kde-format
msgid "Safely remove"
msgstr "Безопасно отстраняване"

#: package/contents/ui/DeviceItem.qml:279
#, kde-format
msgid "Mount"
msgstr "Монтиране"

#: package/contents/ui/FullRepresentation.qml:43
#: package/contents/ui/main.qml:252
#, kde-format
msgid "Remove All"
msgstr "Премахване на всички"

#: package/contents/ui/FullRepresentation.qml:44
#, kde-format
msgid "Click to safely remove all devices"
msgstr "Щракнете, за да премахнете безопасно всички устройства"

#: package/contents/ui/FullRepresentation.qml:185
#, kde-format
msgid "No removable devices attached"
msgstr "Няма свързани сменяеми устройства"

#: package/contents/ui/FullRepresentation.qml:185
#, kde-format
msgid "No disks available"
msgstr "Няма налични дискове"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "Most Recent Device"
msgstr "Най-ново устройство"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "No Devices Available"
msgstr "Няма налични устройства"

#: package/contents/ui/main.qml:234
#, kde-format
msgctxt "Open auto mounter kcm"
msgid "Configure Removable Devices…"
msgstr "Конфигуриране на сменяеми устройства…"

#: package/contents/ui/main.qml:260
#, kde-format
msgid "Removable Devices"
msgstr "Сменяеми устройства"

#: package/contents/ui/main.qml:275
#, kde-format
msgid "Non Removable Devices"
msgstr "Несменяеми устройства"

#: package/contents/ui/main.qml:290
#, kde-format
msgid "All Devices"
msgstr "Всички устройства"

#: package/contents/ui/main.qml:307
#, kde-format
msgid "Show popup when new device is plugged in"
msgstr "Показване на известие при свързване на ново устройство"
