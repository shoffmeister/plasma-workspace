# Translation of plasma_applet_org.kde.plasma.private.systemtray to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2015, 2016, 2017, 2019, 2020, 2021, 2022.
# Øystein Steffensen-Alværvik <oysteins.omsetting@protonmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-06 01:39+0000\n"
"PO-Revision-Date: 2022-08-03 13:59+0200\n"
"Last-Translator: Øystein Steffensen-Alværvik <oysteins.omsetting@protonmail."
"com>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.3\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Generelt"

#: package/contents/config/config.qml:18
#, kde-format
msgid "Entries"
msgstr "Oppføringar"

#: package/contents/ui/ConfigEntries.qml:33
#, kde-format
msgid "Application Status"
msgstr "Programstatus"

#: package/contents/ui/ConfigEntries.qml:35
#, kde-format
msgid "Communications"
msgstr "Kommunikasjon"

#: package/contents/ui/ConfigEntries.qml:37
#, kde-format
msgid "System Services"
msgstr "Systemtenester"

#: package/contents/ui/ConfigEntries.qml:39
#, kde-format
msgid "Hardware Control"
msgstr "Maskinvarekontroll"

#: package/contents/ui/ConfigEntries.qml:42
#, kde-format
msgid "Miscellaneous"
msgstr "Ymse"

#: package/contents/ui/ConfigEntries.qml:74
#, kde-format
msgctxt "Name of the system tray entry"
msgid "Entry"
msgstr "Oppføring"

#: package/contents/ui/ConfigEntries.qml:80
#, kde-format
msgid "Visibility"
msgstr "Synlegheit"

#: package/contents/ui/ConfigEntries.qml:86
#, kde-format
msgid "Keyboard Shortcut"
msgstr "Snøggtast"

#: package/contents/ui/ConfigEntries.qml:238
#, kde-format
msgid "Shown when relevant"
msgstr "Vis når relevant"

#: package/contents/ui/ConfigEntries.qml:239
#, kde-format
msgid "Always shown"
msgstr "Vis alltid"

#: package/contents/ui/ConfigEntries.qml:240
#, kde-format
msgid "Always hidden"
msgstr "Gøym alltid"

#: package/contents/ui/ConfigEntries.qml:241
#, kde-format
msgid "Disabled"
msgstr "Slått av"

#: package/contents/ui/ConfigEntries.qml:305
#, kde-format
msgid "Always show all entries"
msgstr "Vis alltid alle oppføringane"

#: package/contents/ui/ConfigGeneral.qml:24
#, kde-format
msgctxt "The arrangement of system tray icons in the Panel"
msgid "Panel icon size:"
msgstr "Storleik på panelikon:"

#: package/contents/ui/ConfigGeneral.qml:26
#, kde-format
msgid "Small"
msgstr "Liten"

#: package/contents/ui/ConfigGeneral.qml:33
#, kde-format
msgid "Scale with Panel height"
msgstr "Skaler med panelhøgda"

#: package/contents/ui/ConfigGeneral.qml:34
#, kde-format
msgid "Scale with Panel width"
msgstr "Skaler med panelbreidda"

#: package/contents/ui/ConfigGeneral.qml:40
#, kde-format
msgid "Automatically enabled when in Touch Mode"
msgstr "Vert automatisk brukt i trykkskjerm-modus"

#: package/contents/ui/ConfigGeneral.qml:49
#, kde-format
msgctxt "@label:listbox The spacing between system tray icons in the Panel"
msgid "Panel icon spacing:"
msgstr "Mellomrom mellom panelikon:"

#: package/contents/ui/ConfigGeneral.qml:52
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Small"
msgstr "Lite"

#: package/contents/ui/ConfigGeneral.qml:56
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Normal"
msgstr "Vanleg"

#: package/contents/ui/ConfigGeneral.qml:60
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Large"
msgstr "Stort"

#: package/contents/ui/ConfigGeneral.qml:83
#, kde-format
msgctxt "@info:usagetip under a combobox when Touch Mode is on"
msgid "Automatically set to Large when in Touch Mode"
msgstr "Set automatisk til «stort» i trykkskjerm-modus"

#: package/contents/ui/ExpandedRepresentation.qml:62
#, kde-format
msgctxt "@action:button"
msgid "Go Back"
msgstr "Gå tilbake"

#: package/contents/ui/ExpandedRepresentation.qml:74
#, kde-format
msgid "Status and Notifications"
msgstr "Status og varslingar"

#: package/contents/ui/ExpandedRepresentation.qml:140
#, kde-format
msgid "More actions"
msgstr "Fleire handlingar"

#: package/contents/ui/ExpandedRepresentation.qml:234
#, kde-format
msgid "Keep Open"
msgstr "Hald open"

#: package/contents/ui/ExpanderArrow.qml:24
#, kde-format
msgid "Expand System Tray"
msgstr "Utvid systramtrauet"

#: package/contents/ui/ExpanderArrow.qml:25
#, kde-format
msgid "Show all the items in the system tray in a popup"
msgstr "Vis alle ikona i systemtrauet i ein sprettopp"

#: package/contents/ui/ExpanderArrow.qml:38
#, kde-format
msgid "Close popup"
msgstr "Lukk sprettopp"

#: package/contents/ui/ExpanderArrow.qml:38
#, kde-format
msgid "Show hidden icons"
msgstr "Vis gøymde ikon"
